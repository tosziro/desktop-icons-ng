# Dutch translation for desktop-icons.
# Copyright (C) 2019 desktop-icons's COPYRIGHT HOLDER
# This file is distributed under the same license as the desktop-icons package.
# Nathan Follens <nthn@unseen.is>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: desktop-icons master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-28 22:17+0200\n"
"PO-Revision-Date: 2019-03-04 19:46+0100\n"
"Last-Translator: Nathan Follens <nthn@unseen.is>\n"
"Language-Team: Dutch <gnome-nl-list@gnome.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.2.1\n"

#: askConfirmPopup.js:35 askNamePopup.js:36 desktopIconsUtil.js:190
msgid "Cancel"
msgstr "Annuleren"

#: askConfirmPopup.js:36
msgid "Delete"
msgstr ""

#: askNamePopup.js:35
msgid "OK"
msgstr "Oké"

#: askRenamePopup.js:40
#, fuzzy
msgid "Folder name"
msgstr "Nieuwe mapnaam"

#: askRenamePopup.js:40
#, fuzzy
msgid "File name"
msgstr "Hernoemen…"

#: askRenamePopup.js:47
#, fuzzy
msgid "Rename"
msgstr "Hernoemen…"

#: desktopIconsUtil.js:74
msgid "Command not found"
msgstr ""

#: desktopIconsUtil.js:181
msgid "Do you want to run “{0}”, or display its contents?"
msgstr ""

#: desktopIconsUtil.js:182
msgid "“{0}” is an executable text file."
msgstr ""

#: desktopIconsUtil.js:186
#, fuzzy
msgid "Execute in a terminal"
msgstr "Openen in terminalvenster"

#: desktopIconsUtil.js:188
msgid "Show"
msgstr ""

#: desktopIconsUtil.js:192
msgid "Execute"
msgstr ""

#: desktopManager.js:104
msgid "Nautilus File Manager not found"
msgstr ""

#: desktopManager.js:105
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr ""

#: desktopManager.js:420
msgid "New Folder"
msgstr "Nieuwe map"

#: desktopManager.js:424
msgid "New Document"
msgstr ""

#: desktopManager.js:429
msgid "Paste"
msgstr "Plakken"

#: desktopManager.js:433
msgid "Undo"
msgstr "Ongedaan maken"

#: desktopManager.js:437
msgid "Redo"
msgstr "Opnieuw"

#: desktopManager.js:443
msgid "Select all"
msgstr ""

#: desktopManager.js:449
msgid "Show Desktop in Files"
msgstr "Bureaublad tonen in Bestanden"

#: desktopManager.js:453 fileItem.js:790
msgid "Open in Terminal"
msgstr "Openen in terminalvenster"

#: desktopManager.js:459
msgid "Change Background…"
msgstr "Achtergrond aanpassen…"

#: desktopManager.js:468
msgid "Display Settings"
msgstr "Scherminstellingen"

#: desktopManager.js:475 preferences.js:83
msgid "Settings"
msgstr "Instellingen"

#: desktopManager.js:926 desktopManager.js:974
msgid "Error while deleting files"
msgstr ""

#: desktopManager.js:1000
msgid "Are you sure you want to permanently delete these items?"
msgstr ""

#: desktopManager.js:1001
msgid "If you delete an item, it will be permanently lost."
msgstr ""

#: desktopManager.js:1093
#, fuzzy
msgid "New folder"
msgstr "Nieuwe map"

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: fileItem.js:144
msgid "Home"
msgstr ""

#: fileItem.js:646
msgid "Don’t Allow Launching"
msgstr "Toepassingen starten niet toestaan"

#: fileItem.js:648
msgid "Allow Launching"
msgstr "Toepassingen starten toestaan"

#: fileItem.js:714
msgid "Open"
msgstr "Openen"

#: fileItem.js:720
msgid "Open With Other Application"
msgstr "Met andere toepassing openen"

#: fileItem.js:727
msgid "Cut"
msgstr "Knippen"

#: fileItem.js:730
msgid "Copy"
msgstr "Kopiëren"

#: fileItem.js:734
msgid "Rename…"
msgstr "Hernoemen…"

#: fileItem.js:738
msgid "Move to Trash"
msgstr "Verplaatsen naar prullenbak"

#: fileItem.js:742
msgid "Delete permanently"
msgstr ""

#: fileItem.js:755
msgid "Empty Trash"
msgstr "Prullenbak legen"

#: fileItem.js:762
msgid "Eject"
msgstr ""

#: fileItem.js:769
msgid "Unmount"
msgstr ""

#: fileItem.js:782
msgid "Properties"
msgstr "Eigenschappen"

#: fileItem.js:786
msgid "Show in Files"
msgstr "Tonen in Bestanden"

#: preferences.js:89
msgid "Size for the desktop icons"
msgstr "Grootte van bureaubladpictogrammen"

#: preferences.js:89
msgid "Tiny"
msgstr ""

#: preferences.js:89
msgid "Small"
msgstr "Klein"

#: preferences.js:89
msgid "Standard"
msgstr "Standaard"

#: preferences.js:89
msgid "Large"
msgstr "Groot"

#: preferences.js:90
msgid "Show the personal folder in the desktop"
msgstr "Toon de persoonlijke map op het bureaublad"

#: preferences.js:91
msgid "Show the trash icon in the desktop"
msgstr "Toon het prullenbakpictogram op het bureaublad"

#: preferences.js:92 schemas/org.gnome.shell.extensions.ding.gschema.xml:38
#, fuzzy
msgid "Show external drives in the desktop"
msgstr "Toon de persoonlijke map op het bureaublad"

#: preferences.js:93 schemas/org.gnome.shell.extensions.ding.gschema.xml:43
#, fuzzy
msgid "Show network drives in the desktop"
msgstr "Toon de persoonlijke map op het bureaublad."

#: preferences.js:96
#, fuzzy
msgid "New icons alignment"
msgstr "Pictogramgrootte"

#: preferences.js:97
msgid "Top-left corner"
msgstr ""

#: preferences.js:98
msgid "Top-right corner"
msgstr ""

#: preferences.js:99
msgid "Bottom-left corner"
msgstr ""

#: preferences.js:100
msgid "Bottom-right corner"
msgstr ""

#: preferences.js:102 schemas/org.gnome.shell.extensions.ding.gschema.xml:48
msgid "Add new drives to the opposite side of the screen"
msgstr ""

#: preferences.js:106
msgid "Settings shared with Nautilus"
msgstr ""

#: preferences.js:112
msgid "Click type for open files"
msgstr ""

#: preferences.js:112
msgid "Single click"
msgstr ""

#: preferences.js:112
msgid "Double click"
msgstr ""

#: preferences.js:113
#, fuzzy
msgid "Show hidden files"
msgstr "Tonen in Bestanden"

#: preferences.js:114
msgid "Show a context menu item to delete permanently"
msgstr ""

#: preferences.js:117
msgid "Action to do when launching a program from the desktop"
msgstr ""

#: preferences.js:118
msgid "Display the content of the file"
msgstr ""

#: preferences.js:119
msgid "Launch the file"
msgstr ""

#: preferences.js:120
msgid "Ask what to do"
msgstr ""

#: preferences.js:124
msgid "Show image thumbnails"
msgstr ""

#: preferences.js:125
msgid "Never"
msgstr ""

#: preferences.js:126
msgid "Local files only"
msgstr ""

#: preferences.js:127
msgid "Always"
msgstr ""

#: prefs.js:39
msgid ""
"To configure Desktop Icons NG, do right-click in the desktop and choose the "
"last item: 'Settings'"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:18
msgid "Icon size"
msgstr "Pictogramgrootte"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:19
msgid "Set the size for the desktop icons."
msgstr "Stel de grootte van de bureaubladpictogrammen in."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:23
msgid "Show personal folder"
msgstr "Persoonlijke map tonen"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:24
msgid "Show the personal folder in the desktop."
msgstr "Toon de persoonlijke map op het bureaublad."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:28
msgid "Show trash icon"
msgstr "Prullenbakpictogram tonen"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:29
msgid "Show the trash icon in the desktop."
msgstr "Toon het prullenbakpictogram op het bureaublad."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:33
#, fuzzy
msgid "New icons start corner"
msgstr "Pictogramgrootte"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:34
msgid "Set the corner from where the icons will start to be placed."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:39
msgid "Show the disk drives connected to the computer."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:44
#, fuzzy
msgid "Show mounted network volumes in the desktop."
msgstr "Toon de persoonlijke map op het bureaublad."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:49
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""

#, fuzzy
#~ msgid "Show external disk drives in the desktop"
#~ msgstr "Toon de persoonlijke map op het bureaublad"

#, fuzzy
#~ msgid "Show the external drives"
#~ msgstr "Toon de persoonlijke map op het bureaublad"

#, fuzzy
#~ msgid "Show network volumes"
#~ msgstr "Tonen in Bestanden"

#~ msgid "Enter file name…"
#~ msgstr "Voer bestandsnaam in…"

#~ msgid "Create"
#~ msgstr "Aanmaken"

#~ msgid "Folder names cannot contain “/”."
#~ msgstr "Mapnamen kunnen geen ‘/’ bevatten."

#~ msgid "A folder cannot be called “.”."
#~ msgstr "Een map kan niet ‘.’ worden genoemd."

#~ msgid "A folder cannot be called “..”."
#~ msgstr "Een map kan niet ‘..’ worden genoemd."

#~ msgid "Folders with “.” at the beginning of their name are hidden."
#~ msgstr "Mappen waarvan de naam begint met ‘.’ zijn verborgen."

#~ msgid "There is already a file or folder with that name."
#~ msgstr "Er bestaat al een bestand of map met die naam."
